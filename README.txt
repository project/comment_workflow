

Description
-------------
Comment Workflow closes comments if the node state is changed to the last state
of a workflow and re-opens comments if the state is changed from the last 
state. Comment Workflow will make these changes if:

  * Comment Workflow is enabled for the node type
  * The node's comment setting is Read only or Read/Write (Comment Workflow will 
    not alter a Disabled comment setting)
  * A workflow with three or more states (including creation) is assigned to 
    the node type 


Dependencies
-------------
Comment and Workflow modules


Installation
-------------
1. copy the comment_workflow directory and all its contents to your modules 
   directory
2. enable the module: admin/build/modules
3. enable Comment Workflow on one or more node types


Download
-------------
Download package and report bugs, feature requests, or submit a patch from the 
project page on the Drupal web site.
http://drupal.org/project/comment_workflow


Developers
-------------
The node content type form has become cluttered with all of the additional 
settings introduced. Comment-related node content type settings could be 
grouped in it's own fieldset, comment_options. This module includes a code 
block to create the fieldset if it does not already exist and move the Default 
Comment setting to this fieldset which will make the form more intuitive until 
a standard arises. Feel free to copy the code block into your own modules.


Todo List
-------------
None


Author
-------------
John Money
ossemble LLC.
http://ossemble.com

Module development sponsored by LifeWire, a subsidiary of The New York Times 
Company.
http://www.lifewire.com
